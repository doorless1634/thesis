package controller.editor;

import java.lang.invoke.MethodHandles;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.kordamp.ikonli.javafx.FontIcon;

import controller.editor.data.ConstraintDataRow;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.Separator;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import task.editor.TaskType;
import task.editor.TextInputTask;
import task.editor.VoiceInputTask;
import task.editor.VoiceInputTaskResult;
import treedata.TreeDataActivity;
import util.ConstraintUtils;
import util.ModelUtils;

public class VoiceInputChatBox {

	private static final Logger logger = LogManager.getLogger(MethodHandles.lookup().lookupClass());

	private ListView<HBox> chatListView;

	private EditorTabController editorTabController;

	private List<TreeDataActivity> currentRecordedActivities = new ArrayList<>();
	private List<ConstraintDataRow> currentRecordedConstraints = new ArrayList<>();

	private boolean activationConditionDone;
	private boolean correlationConditionDone;
	private boolean timeConditionDone;

	private ExecutorService executorService = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

	public VoiceInputChatBox() {
		chatListView = new ListView<>();
		chatListView.getStyleClass().add("chat-box");
		writeIntroduction();
		askForConstraint(false);
	}

	public ListView<HBox> getRoot() {
		return chatListView;
	}

	private void writeIntroduction() {
		writeSystemMessage("Hi! My name is Declo.", false);
		writeSystemMessage("I can help you create a model based on sentences you say or write.", false);
	}

	private void askForConstraint(boolean insertSeparator) {
		if (insertSeparator) {
			Separator separator = new Separator();
			HBox.setHgrow(separator, Priority.ALWAYS);
			chatListView.getItems().add(new HBox(separator));
		}
		writeSystemMessage("Let me know when I can start listening for a constraint.", false);
		writeSystemMessage("Or you can write a sentence instead.", false);

		activationConditionDone = false;
		correlationConditionDone = false;
		timeConditionDone = false;
		currentRecordedActivities.clear();
		currentRecordedConstraints.clear();

		createConstraintInputRow();
	}

	private void createConstraintInputRow() {
		HBox messageRow = createUserMessageRow();
		Button constraintRecordButton = createRecordButton("Start listening", messageRow, TaskType.CONSTRAINT);
		Button writeButton = createWriteButton("Let's write", messageRow, TaskType.CONSTRAINT);
		messageRow.getChildren().setAll(constraintRecordButton, writeButton);
	}

	private void startRecording(TaskType taskType) {
		writeSystemMessage("I'm listening...", false);
		writeSystemMessage("(I will stop after I detect a sentence)", true);

		Button cancelRecordButton = createCancelRecordButton();
		HBox messageRow = createUserMessageRow(cancelRecordButton);
		VoiceInputTask voiceInputTask = createVoiceInputTask(taskType, messageRow);
		cancelRecordButton.setOnAction(event -> {
			updateUserMessage(cancelRecordButton.getText(), messageRow, false);
			voiceInputTask.cancel(true);
			writeSystemMessage("I have stopped listening.", false);
			if (taskType == TaskType.CONSTRAINT) {
				askForConstraint(false);
			} else {
				askForConditionType(true);
			}
		});

		executorService.execute(voiceInputTask);
	}

	private void processRecording(VoiceInputTaskResult voiceInputTaskResult) {
		switch (voiceInputTaskResult.getTaskType()) {
		case CONSTRAINT:
			processConstraintTaskType(voiceInputTaskResult.getResultString());
			break;
		case ACTIVATION_CONDITION:
			processActivationTaskType(voiceInputTaskResult.getResultString());
			break;
		case CORRELATION_CONDITION:
			processCorrelationTaskType(voiceInputTaskResult.getResultString());
			break;
		case TIME_CONDITION:
			processTimeTaskType(voiceInputTaskResult.getResultString());
			break;
		default:
			logger.error("Unhandled voice input task type for processing recording: {}", voiceInputTaskResult.getTaskType());
			break;
		}

		askForRecordingConfirmation(voiceInputTaskResult);
	}

	private void askForRecordingConfirmation(VoiceInputTaskResult voiceInputTaskResult) {
		writeSystemMessage("Did I understand you correctly?", false);

		Button yesButton = new Button("Yes");
		yesButton.getStyleClass().add("small-button");
		Button noButton = new Button("No");
		noButton.getStyleClass().add("small-button");

		HBox messageRow = createUserMessageRow(yesButton, noButton);

		yesButton.setOnAction(event -> {
			updateUserMessage(yesButton.getText(), messageRow, false);
			if (voiceInputTaskResult.getTaskType() == TaskType.CONSTRAINT) {
				askForCondition();
			} else {
				if (voiceInputTaskResult.getTaskType() == TaskType.ACTIVATION_CONDITION) {
					activationConditionDone = true;
				} else if (voiceInputTaskResult.getTaskType() == TaskType.CORRELATION_CONDITION) {
					correlationConditionDone = true;
				} else if (voiceInputTaskResult.getTaskType() == TaskType.TIME_CONDITION) {
					timeConditionDone = true;
				} else {
					logger.warn("Unhandled voice input task type for confirming recording: {}", voiceInputTaskResult.getTaskType());
				}
				askForConditionType(true);
			}
		});

		noButton.setOnAction(event -> {
			updateUserMessage(noButton.getText(), messageRow, false);
			askForRecordingCorrection(voiceInputTaskResult);
		});
	}

	private void askForCondition() {
		writeSystemMessage("Would you like to record or write conditions?", false);

		Button recordButton = new Button("Record");
		recordButton.getStyleClass().add("small-button");
		Button writeButton = new Button("Write");
		writeButton.getStyleClass().add("small-button");
		Button noButton = new Button("No");
		noButton.getStyleClass().add("small-button");

		HBox messageRow = createUserMessageRow(recordButton, writeButton, noButton);

		recordButton.setOnAction(event -> {
			updateUserMessage(recordButton.getText(), messageRow, false);
			askForConditionType(true);
		});

		writeButton.setOnAction(event -> {
			updateUserMessage(writeButton.getText(), messageRow, false);
			askForConditionType(false);
		});

		noButton.setOnAction(event -> {
			updateUserMessage(noButton.getText(), messageRow, false);
			askForConstraint(true);
		});

	}

	private void askForConditionType(boolean record) {
		if (activationConditionDone && (correlationConditionDone || !binaryTemplatesRecorded()) && timeConditionDone) {
			writeSystemMessage("All condition types are added.", false);
			askForConstraint(true);
		} else {
			if (activationConditionDone || correlationConditionDone || timeConditionDone) {
				if (record) {
					writeSystemMessage("Would you like to record another type of condition?", false);
				} else {
					writeSystemMessage("Would you like to write another type of condition?", false);
				}
			} else {
				if (record) {
					writeSystemMessage("Which type of condition would you like to record?", false);
				} else {
					writeSystemMessage("Which type of condition would you like to write?", false);
				}
			}

			createConditionInputRow(record);
		}
	}

	private void createConditionInputRow(boolean record) {
		HBox outerMessageRow = createUserMessageRow();

		VBox subRowsContainer = new VBox();
		subRowsContainer.setStyle("-fx-spacing: 4;");
		outerMessageRow.getChildren().add(subRowsContainer);

		HBox firstInnerMessageRow = new HBox();
		firstInnerMessageRow.setStyle("-fx-alignment: center_right; -fx-spacing: 3;");
		subRowsContainer.getChildren().add(firstInnerMessageRow);

		if (!activationConditionDone) {
			Button activationButton;
			if (record) {
				activationButton = createRecordButton("Activation", outerMessageRow, TaskType.ACTIVATION_CONDITION);
			} else {
				activationButton = createWriteButton("Activation", outerMessageRow, TaskType.ACTIVATION_CONDITION);
			}
			firstInnerMessageRow.getChildren().add(activationButton);
		}
		if (!correlationConditionDone && binaryTemplatesRecorded()) {
			Button correlationButton;
			if (record) {
				correlationButton = createRecordButton("Correlation", outerMessageRow, TaskType.CORRELATION_CONDITION);
			} else {
				correlationButton = createWriteButton("Correlation", outerMessageRow, TaskType.CORRELATION_CONDITION);
			}
			firstInnerMessageRow.getChildren().add(correlationButton);
		}
		if (!timeConditionDone) {
			Button timeButton;
			if (record) {
				timeButton = createRecordButton("Time", outerMessageRow, TaskType.TIME_CONDITION);
			} else {
				timeButton = createWriteButton("Time", outerMessageRow, TaskType.TIME_CONDITION);
			}
			firstInnerMessageRow.getChildren().add(timeButton);
		}
		if (activationConditionDone || correlationConditionDone || timeConditionDone) {
			Button noButton = new Button("No");
			noButton.getStyleClass().add("small-button");
			firstInnerMessageRow.getChildren().add(noButton);
			noButton.setOnAction(event -> {
				updateUserMessage(noButton.getText(), outerMessageRow, false);
				askForConstraint(true);
			});
		} else {
			HBox secondInnerMessageRow = new HBox();
			secondInnerMessageRow.setStyle("-fx-alignment: center_right;");
			subRowsContainer.getChildren().add(secondInnerMessageRow);

			Button backButton = new Button("I changed my mind");
			backButton.getStyleClass().add("small-button");
			secondInnerMessageRow.getChildren().add(backButton);

			backButton.setOnAction(event -> {
				updateUserMessage(backButton.getText(), outerMessageRow, false);
				askForCondition();
			});
		}
	}

	private boolean binaryTemplatesRecorded() {
		for (ConstraintDataRow constraintDataRow : currentRecordedConstraints) {
			if (constraintDataRow.getTemplate().getIsBinary()) {
				return true;
			}
		}
		return false;
	}

	private void askForRecordingCorrection(VoiceInputTaskResult voiceInputTaskResult) {
		writeSystemMessage("Would you like to edit the text, record again or discard the results?", false);

		HBox messageRow = createUserMessageRow();
		Button editButton = new Button("Edit");
		editButton.getStyleClass().add("small-button");
		Button recordButton = createRecordButton("Record", messageRow, voiceInputTaskResult.getTaskType());
		Button discardButton = new Button("Discard");
		discardButton.getStyleClass().add("small-button");

		messageRow.getChildren().setAll(editButton, recordButton, discardButton);

		editButton.setOnAction(event -> {
			askForTextualInput(voiceInputTaskResult.getTaskType(), voiceInputTaskResult.getRecordedString());
			updateUserMessage(editButton.getText(), messageRow, false);
		});

		discardButton.setOnAction(event -> {
			updateUserMessage(discardButton.getText(), messageRow, false);
			dicardRecording(voiceInputTaskResult);
			if (voiceInputTaskResult.getTaskType() == TaskType.CONSTRAINT) {
				askForConstraint(false);
			} else {
				askForConditionType(true);
			}
		});
	}


	private void askForTextualInput(TaskType taskType, String initialText) {
		writeSystemMessage("Please type the sentence.", false);

		TextArea editInput = new TextArea(initialText);
		editInput.setStyle("-fx-max-height: 60px;");
		editInput.setWrapText(true);
		Button applyButton = new Button("Apply");
		applyButton.getStyleClass().add("small-button");
		Button doneButton = new Button("Done");
		doneButton.getStyleClass().add("small-button");
		HBox buttonsRow = new HBox(applyButton, doneButton);
		buttonsRow.setStyle("-fx-alignment: center_right;");
		buttonsRow.setSpacing(3d);
		VBox editingBox = new VBox(editInput, buttonsRow);

		HBox messageRow = createUserMessageRow(editingBox);

		applyButton.setOnAction(event -> {
			TextInputTask textInputTask = createTextInputTask(editInput.getText(), messageRow, taskType, false);
			textInputTask.runningProperty().addListener((obs, oldVal, newVal) -> {
				messageRow.setDisable(newVal);
				if (newVal.equals(Boolean.FALSE))
					editInput.requestFocus();
			});
			executorService.execute(textInputTask);
		});

		doneButton.setOnAction(event -> {
			TextInputTask textInputTask = createTextInputTask(editInput.getText(), messageRow, taskType, true);
			textInputTask.runningProperty().addListener( (obs, oldVal, newVal) -> messageRow.setDisable(newVal) );
			executorService.execute(textInputTask);
		});

		ChangeListener<Scene> sceneListener = new ChangeListener<Scene>() {
			@Override
			public void changed(ObservableValue<? extends Scene> observable, Scene oldValue, Scene newValue) {
				if (newValue != null) {
					editInput.requestFocus();
					editInput.sceneProperty().removeListener(this);
				}
			}
		};
		editInput.sceneProperty().addListener(sceneListener);
	}

	private TextInputTask createTextInputTask(String editedText, HBox messageRow, TaskType taskType, boolean done) {
		TextInputTask textInputTask = new TextInputTask(editedText, taskType);

		textInputTask.setOnSucceeded(event -> {
			if (done && (textInputTask.getValue() == null || textInputTask.getValue().equals(""))) {
				updateUserMessage(editedText, messageRow, true);
				if (taskType == TaskType.CONSTRAINT) {
					writeSystemMessage("I did not find any constraints from this sentence, but we can try again.", false);
					createConstraintInputRow();
				} else {
					writeSystemMessage("I did not find a condition from this sentence, but we can try again.", false);
					createConditionInputRow(false);
				}
			} else {
				switch (textInputTask.getTaskType()) {
				case CONSTRAINT:
					processConstraintTaskType(textInputTask.getValue());
					break;
				case ACTIVATION_CONDITION:
					processActivationTaskType(textInputTask.getValue());
					break;
				case CORRELATION_CONDITION:
					processCorrelationTaskType(textInputTask.getValue());
					break;
				case TIME_CONDITION:
					processTimeTaskType(textInputTask.getValue());
					break;
				default:
					logger.error("Unhandled task type for creating text input task: {}", textInputTask.getTaskType());
					break;
				}

				if (done) {
					updateUserMessage(editedText, messageRow, true);
					if (taskType == TaskType.CONSTRAINT) {
						askForCondition();
					} else {
						if (taskType == TaskType.ACTIVATION_CONDITION) {
							activationConditionDone = true;
						} else if (taskType == TaskType.CORRELATION_CONDITION) {
							correlationConditionDone = true;
						} else if (taskType == TaskType.TIME_CONDITION) {
							timeConditionDone = true;
						}
						logger.error("Unhandled task type for confirming text input task: {}", textInputTask.getTaskType());
						askForConditionType(false);
					}
				}
			}
		});

		textInputTask.setOnFailed(event -> {
			handleInputTaskFailure(false, taskType, messageRow);
		});

		return textInputTask;
	}

	private void dicardRecording(VoiceInputTaskResult voiceInputTaskResult) {
		switch (voiceInputTaskResult.getTaskType()) {
		case CONSTRAINT:
			removeCurrentRecordedDataRows();
			break;
		case ACTIVATION_CONDITION:
			discardActivationTaskType();
			break;
		case CORRELATION_CONDITION:
			discardCorrelationTaskType();
			break;
		case TIME_CONDITION:
			discardTimeTaskType();
			break;
		default:
			logger.error("Unhandled task type discarding recording: {}", voiceInputTaskResult.getTaskType());
			break;
		}
	}

	private VoiceInputTask createVoiceInputTask(TaskType taskType, HBox messageRow) {
		VoiceInputTask voiceInputTask = new VoiceInputTask(taskType);

		voiceInputTask.setOnSucceeded(event -> {
			String recordedString = voiceInputTask.getValue().getRecordedString();
			updateUserMessage(recordedString, messageRow, true);
			if (voiceInputTask.getValue().getResultString() == null || voiceInputTask.getValue().getResultString().equals("")) {
				if (taskType == TaskType.CONSTRAINT) {
					writeSystemMessage("I did not find any constraints from this sentence, but we can try again.", false);
					createConstraintInputRow();
				} else {
					writeSystemMessage("I did not find a condition from this sentence, but we can try again.", false);
					createConditionInputRow(true);
				}
			} else {
				processRecording(voiceInputTask.getValue());
			}
		});

		voiceInputTask.setOnFailed(event -> handleInputTaskFailure(true, taskType, messageRow) );

		return voiceInputTask;
	}

	private void handleInputTaskFailure(boolean record, TaskType taskType, HBox messageRow) {
		updateUserMessage("", messageRow, false);
		if (record) {
			writeSystemMessage("Unfortunately the recording failed, but we can try again.", false);
		} else {
			writeSystemMessage("Unfortunately the text processing failed, but we can try again.", false);
		}

		if (taskType == TaskType.CONSTRAINT) {
			createConstraintInputRow();
		} else {
			createConditionInputRow(record);
		}
	}

	private Button createRecordButton(String buttonText, HBox messageRow, TaskType taskType) {
		Button button = new Button(buttonText);
		button.getStyleClass().add("small-button");
		FontIcon buttonFontIcon = new FontIcon("fa-microphone");
		buttonFontIcon.getStyleClass().add("small-button__icon");
		button.setGraphic(buttonFontIcon);

		button.setOnAction(event -> {
			updateUserMessage(buttonText, messageRow, false);
			startRecording(taskType);
		});

		return button;
	}

	private Button createWriteButton(String buttonText, HBox messageRow, TaskType taskType) {
		Button button = new Button(buttonText);
		button.getStyleClass().add("small-button");
		FontIcon buttonFontIcon = new FontIcon("fa-pencil");
		buttonFontIcon.getStyleClass().add("small-button__icon");
		button.setGraphic(buttonFontIcon);

		button.setOnAction(event -> {
			updateUserMessage(buttonText, messageRow, false);
			askForTextualInput(taskType, null);
		});

		return button;
	}

	private Button createCancelRecordButton() {
		Button cancelRecordButton = new Button("Cancel");
		cancelRecordButton.getStyleClass().add("small-button");
		FontIcon recordFontIcon = new FontIcon("fa-microphone-slash");
		recordFontIcon.getStyleClass().add("small-button__icon");
		cancelRecordButton.setGraphic(recordFontIcon);
		return cancelRecordButton;
	}

	private void writeSystemMessage(String message, boolean italic) {
		Label messageLabel = new Label(message);
		messageLabel.getStyleClass().add("message-label__system");
		if (italic) {
			messageLabel.setStyle("-fx-font-style: italic;");
		}

		HBox messageRow = new HBox(messageLabel);
		messageRow.getStyleClass().add("message-row__system");
		chatListView.getItems().add(messageRow);
		chatListView.scrollTo(chatListView.getItems().size()-1);
	}

	private HBox createUserMessageRow(Node... children) {
		HBox messageRow = new HBox(children);
		messageRow.getStyleClass().add("message-row__user");
		chatListView.getItems().add(messageRow);
		chatListView.scrollTo(chatListView.getItems().size()-1);
		return messageRow;
	}

	private void updateUserMessage(String message, HBox messageRow, boolean isFreeform) {
		Label messageLabel = new Label();
		if (isFreeform) {
			message = "\"" + message + "\"";
			messageLabel.setStyle("-fx-font-style: italic;");
		}
		messageLabel.setText(message);
		messageLabel.getStyleClass().add("message-label__user");
		messageRow.getChildren().setAll(messageLabel);
	}


	public void setEditorTabController(EditorTabController editorTabController) {
		this.editorTabController = editorTabController;
	}

	private void processConstraintTaskType(String taskResultString) {
		logger.debug("Processing constraint task results: {}", taskResultString);

		removeCurrentRecordedDataRows();

		List<TreeDataActivity> treeDataActivities = new ArrayList<>();
		editorTabController.getActivitiesRoot().getChildren().forEach(item -> treeDataActivities.add((TreeDataActivity)item));

		List<String> activityList = ModelUtils.getActivityList(taskResultString);
		for (String activity : activityList) {
			boolean isDuplicate = false;
			for (TreeDataActivity treeDataActivity : treeDataActivities) {
				if (treeDataActivity.getActivityName().equals(activity)) {
					isDuplicate = true;
				}
			}

			if (isDuplicate) {
				logger.debug("Skipped adding duplicate activity to voice recording layer: {}", activity);
			} else {
				TreeDataActivity treeDataActivity = new TreeDataActivity(activity);
				editorTabController.getActivitiesRoot().getChildren().add(treeDataActivity);
				currentRecordedActivities.add(treeDataActivity);
				treeDataActivities.add(treeDataActivity);
				logger.debug("Added valid activity to voice recording layer: {}", activity);
			}
		}


		List<String> constraintsList = ModelUtils.getConstraintsList(taskResultString);
		for (String constraint : constraintsList) {
			ConstraintDataRow detectedConstraint = ConstraintUtils.getConstraintDataRow(constraint, treeDataActivities);
			if (editorTabController.getConstraintRows().contains(detectedConstraint)) {
				detectedConstraint.invalidateSavedRow();
				logger.debug("Added duplicate constraint to voice recording layer ({}): {}", detectedConstraint.hashCode(), constraint);
			} else if (detectedConstraint.validateRowEdit()) {
				detectedConstraint.confirmRowEdit();
				logger.debug("Added valid constraint to voice recording layer ({}): {}", detectedConstraint.hashCode(), constraint);
			} else {
				logger.debug("Added invalid constraint to voice recording layer ({}): {}", detectedConstraint.hashCode(), constraint);
			}
			editorTabController.getConstraintRows().add(detectedConstraint);
			currentRecordedConstraints.add(detectedConstraint);
		}

		editorTabController.scrollTablesToEnd();
		editorTabController.updateVisualization();

		logger.info("Recording results processed");
	}

	private void processActivationTaskType(String taskResultString) {
		logger.debug("Processing activation task results: {}", taskResultString);
		for (ConstraintDataRow constraintDataRow : currentRecordedConstraints) {
			constraintDataRow.startRowEdit();
			constraintDataRow.activationConditionProperty().setEditingValue(taskResultString);
			if (constraintDataRow.validateRowEdit()) {
				constraintDataRow.confirmRowEdit();

				//Initial implementation for detecting attributes from conditions
//				String conditionAttribute = taskResultString.substring(taskResultString.indexOf(".")+1, taskResultString.indexOf(" "));
//				TreeDataAttribute existingAttribute = null;
//
//				if (unconfirmedAttribute != null) {
//					for (int i = unconfirmedAttribute.getActivitiesUnmodifiable().size()-1; i >= 0; i--) {
//						unconfirmedAttribute.removeActivity(unconfirmedAttribute.getActivitiesUnmodifiable().get(i));
//					}
//				}
//				for (TreeDataAttribute treeDataAttribute : editorTabController.getAllAttributes()) {
//					if (treeDataAttribute.getAttributeName().equals(conditionAttribute)) {
//						existingAttribute = treeDataAttribute;
//						break;
//					}
//				}
//				if (existingAttribute == null) {
//					unconfirmedAttribute = new TreeDataAttribute();
//					unconfirmedAttribute.setAttributeName(conditionAttribute);
//
//					String conditionalPart = taskResultString.substring(taskResultString.indexOf(".")+1 + conditionAttribute.length()).strip();
//					if (conditionalPart.startsWith("is ")) {
//						unconfirmedAttribute.setAttributeType(AttributeType.ENUMERATION);
//						List<String> possibleValues = new ArrayList<String>();
//						possibleValues.add(conditionalPart.substring(3));
//						if (possibleValues.contains("true")) {
//							possibleValues.add("false");
//						} else if (possibleValues.contains("false")) {
//							possibleValues.add("true");
//						}
//						unconfirmedAttribute.setPossibleValues(possibleValues);
//					} else if (conditionalPart.startsWith("is not ")) {
//						unconfirmedAttribute.setAttributeType(AttributeType.ENUMERATION);
//						List<String> possibleValues = new ArrayList<String>();
//						possibleValues.add(conditionalPart.substring(7));
//						if (possibleValues.contains("true")) {
//							possibleValues.add("false");
//						} else if (possibleValues.contains("false")) {
//							possibleValues.add("true");
//						}
//						unconfirmedAttribute.setPossibleValues(possibleValues);
//					} else if (conditionalPart.startsWith("in (")) {
//						unconfirmedAttribute.setAttributeType(AttributeType.ENUMERATION);
//						List<String> possibleValues = Arrays.asList(conditionalPart.substring(4, conditionalPart.length()-1).split(", "));
//						unconfirmedAttribute.setPossibleValues(possibleValues);
//					} else if (conditionalPart.startsWith("not in (")) {
//						unconfirmedAttribute.setAttributeType(AttributeType.ENUMERATION);
//						List<String> possibleValues = Arrays.asList(conditionalPart.substring(8, conditionalPart.length()-1).split(", "));
//						unconfirmedAttribute.setPossibleValues(possibleValues);
//					} else {
//						unconfirmedAttribute.setAttributeType(AttributeType.INTEGER);
//						unconfirmedAttribute.setValueFrom(new BigDecimal(1));
//						unconfirmedAttribute.setValueTo(new BigDecimal(100));
//					}
//					
//					constraintDataRow.getActivationActivity().addAttribute(unconfirmedAttribute);
//					//constraintDataRow.getTargetActivity().addAttribute(unconfirmedAttribute);
//				} else {
//					constraintDataRow.getActivationActivity().addAttribute(existingAttribute);
//					//constraintDataRow.getTargetActivity().addAttribute(existingAttribute);
//				}

			}
		}
		editorTabController.scrollTablesToEnd();
	}

	private void processCorrelationTaskType(String taskResultString) {
		logger.debug("Processing correlation task results: {}", taskResultString);
		for (ConstraintDataRow constraintDataRow : currentRecordedConstraints) {
			if (constraintDataRow.getTemplate().getIsBinary()) {
				constraintDataRow.startRowEdit();
				constraintDataRow.correlationConditionProperty().setEditingValue(taskResultString);
				if (constraintDataRow.validateRowEdit()) {
					constraintDataRow.confirmRowEdit();

					//Initial implementation for detecting attributes from conditions
//					String conditionAttribute = taskResultString.substring(taskResultString.lastIndexOf(" ")+1);
//					TreeDataAttribute existingAttribute = null;
//
//					if (unconfirmedAttribute != null) {
//						for (int i = unconfirmedAttribute.getActivitiesUnmodifiable().size()-1; i >= 0; i--) {
//							unconfirmedAttribute.removeActivity(unconfirmedAttribute.getActivitiesUnmodifiable().get(i));
//						}
//					}
//					for (TreeDataAttribute treeDataAttribute : editorTabController.getAllAttributes()) {
//						if (treeDataAttribute.getAttributeName().equals(conditionAttribute)) {
//							existingAttribute = treeDataAttribute;
//							break;
//						}
//					}
//					if (existingAttribute == null) {
//						unconfirmedAttribute = new TreeDataAttribute();
//						unconfirmedAttribute.setAttributeName(conditionAttribute);
//						unconfirmedAttribute.setAttributeType(AttributeType.INTEGER);
//						unconfirmedAttribute.setValueFrom(new BigDecimal(1));
//						unconfirmedAttribute.setValueTo(new BigDecimal(100));
//						constraintDataRow.getActivationActivity().addAttribute(unconfirmedAttribute);
//						constraintDataRow.getTargetActivity().addAttribute(unconfirmedAttribute);
//					} else {
//						constraintDataRow.getActivationActivity().addAttribute(existingAttribute);
//						constraintDataRow.getTargetActivity().addAttribute(existingAttribute);
//					}
				}
			}
		}
		editorTabController.scrollTablesToEnd();
	}

	private void processTimeTaskType(String taskResultString) {
		logger.debug("Processing time task results: {}", taskResultString);
		for (ConstraintDataRow constraintDataRow : currentRecordedConstraints) {
			constraintDataRow.startRowEdit();
			constraintDataRow.timeConditionProperty().setEditingValue(taskResultString);
			if (constraintDataRow.validateRowEdit()) {
				constraintDataRow.confirmRowEdit();
			}
		}
		editorTabController.scrollTablesToEnd();
	}

	private void removeCurrentRecordedDataRows() {
		for (ConstraintDataRow recordedConstraint : currentRecordedConstraints) {
			for (int i = editorTabController.getConstraintRows().size()-1; i >= 0; i--) {
				if (editorTabController.getConstraintRows().get(i)==recordedConstraint) {
					//Checking by reference to make sure no wrong rows are removed
					editorTabController.getConstraintRows().remove(i);
				}
			}
		}
		currentRecordedConstraints.clear();

		for (TreeDataActivity recordedActivity : currentRecordedActivities) {
			editorTabController.getActivitiesRoot().getChildren().remove(recordedActivity);
		}
		currentRecordedActivities.clear();

		//TODO: To be removed after changing the visualisation mechanism
		editorTabController.updateVisualization();
	}

	private void discardActivationTaskType() {
		for (ConstraintDataRow constraintDataRow : currentRecordedConstraints) {
			constraintDataRow.startRowEdit();
			constraintDataRow.activationConditionProperty().setEditingValue(null);
			if (constraintDataRow.validateRowEdit()) {
				constraintDataRow.confirmRowEdit();
			}
		}
	}

	private void discardCorrelationTaskType() {
		for (ConstraintDataRow constraintDataRow : currentRecordedConstraints) {
			constraintDataRow.startRowEdit();
			constraintDataRow.correlationConditionProperty().setEditingValue(null);
			if (constraintDataRow.validateRowEdit()) {
				constraintDataRow.confirmRowEdit();
			}
		}
	}

	private void discardTimeTaskType() {
		for (ConstraintDataRow constraintDataRow : currentRecordedConstraints) {
			constraintDataRow.startRowEdit();
			constraintDataRow.timeConditionProperty().setEditingValue(null);
			if (constraintDataRow.validateRowEdit()) {
				constraintDataRow.confirmRowEdit();
			}
		}
	}
}
