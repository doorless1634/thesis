(define (problem Align) (:domain Mining)
(:objects
t0 - state
t1 - state
t2 - state
t3 - state
t4 - state
t5 - state
t6 - state
t7 - state
t8 - state
t9 - state
t10 - state
s_0_0 - state
s_0_1 - state
)
(:init
(currstate t0)
(currstate s_0_0)
)
(:goal
(and
(currstate t10)
(currstate s_0_0)
))
)