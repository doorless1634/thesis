(define (domain Mining)
(:requirements :typing :equality)
(:types state)

(:predicates
(currstate ?s - state)
)

(:action sync-alo_complete-t0t1
:precondition (currstate t0) 
:effect (and (not (currstate t0)) (currstate t1)))

(:action del-alo_complete-t0-t1
:precondition (currstate t0)
:effect (and (not (currstate t0)) (currstate t1) )
)

(:action sync-mina_complete-t1t2
:precondition (currstate t1) 
:effect (and (not (currstate t1)) (currstate t2)))

(:action del-mina_complete-t1-t2
:precondition (currstate t1)
:effect (and (not (currstate t1)) (currstate t2) )
)

(:action sync-mina_complete-t2t3
:precondition (currstate t2) 
:effect (and (not (currstate t2)) (currstate t3)))

(:action del-mina_complete-t2-t3
:precondition (currstate t2)
:effect (and (not (currstate t2)) (currstate t3) )
)

(:action sync-mina_complete-t3t4
:precondition (currstate t3) 
:effect (and (not (currstate t3)) (currstate t4)))

(:action del-mina_complete-t3-t4
:precondition (currstate t3)
:effect (and (not (currstate t3)) (currstate t4) )
)

(:action sync-alo_complete-t4t5
:precondition (currstate t4) 
:effect (and (not (currstate t4)) (currstate t5)))

(:action del-alo_complete-t4-t5
:precondition (currstate t4)
:effect (and (not (currstate t4)) (currstate t5) )
)

(:action sync-alo_complete-t5t6
:precondition (currstate t5) 
:effect (and (not (currstate t5)) (currstate t6)))

(:action del-alo_complete-t5-t6
:precondition (currstate t5)
:effect (and (not (currstate t5)) (currstate t6) )
)

(:action sync-alo_complete-t6t7
:precondition (currstate t6) 
:effect (and (not (currstate t6)) (currstate t7)))

(:action del-alo_complete-t6-t7
:precondition (currstate t6)
:effect (and (not (currstate t6)) (currstate t7) )
)

(:action sync-alo_complete-t7t8
:precondition (currstate t7) 
:effect (and (not (currstate t7)) (currstate t8)))

(:action del-alo_complete-t7-t8
:precondition (currstate t7)
:effect (and (not (currstate t7)) (currstate t8) )
)

(:action sync-alo_complete-t8t9
:precondition (currstate t8) 
:effect (and (not (currstate t8)) (currstate t9)))

(:action del-alo_complete-t8-t9
:precondition (currstate t8)
:effect (and (not (currstate t8)) (currstate t9) )
)

(:action sync-alo_complete-t9t10
:precondition (currstate t9) 
:effect (and (not (currstate t9)) (currstate t10)))

(:action del-alo_complete-t9-t10
:precondition (currstate t9)
:effect (and (not (currstate t9)) (currstate t10) )
)

(:action goto-abstract_states-cs0
:precondition (and (currstate t10) (currstate s_0_0) )
:effect (and )
)

)