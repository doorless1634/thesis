(define (domain Mining)
(:requirements :typing :equality)
(:types state)

(:predicates
(currstate ?s - state)
)

(:functions
(total-cost)
)

(:action add-getting_license-ct0
:precondition (currstate s_0_1) 
:effect (and (not (currstate s_0_1)) (currstate s_0_0)  (increase (total-cost) )

(:action add-test_failed-ct1
:precondition (currstate s_2_1) 
:effect (and (not (currstate s_2_1)) (currstate s_2_0)  (increase (total-cost) )

(:action add-drivingtest-ct2
:precondition (and (currstate s_4_2) (not (currstate s_0_0)) (not (currstate s_1_0)) (not (currstate s_2_0)) (not (currstate s_3_1)) )
:effect (and (not (currstate s_4_2)) (currstate s_4_0)  (increase (total-cost) )

(:action add-drivingtest-ct3
:precondition (and (currstate s_0_0) (not (currstate s_4_2)) (not (currstate s_1_0)) (not (currstate s_2_0)) (not (currstate s_3_1)) )
:effect (and (not (currstate s_0_0)) (currstate s_0_1)  (increase (total-cost) )

(:action add-drivingtest-ct4
:precondition (and (currstate s_1_0) (not (currstate s_4_2)) (not (currstate s_0_0)) (not (currstate s_2_0)) (not (currstate s_3_1)) )
:effect (and (not (currstate s_1_0)) (currstate s_1_1)  (increase (total-cost) )

(:action add-drivingtest-ct5
:precondition (and (currstate s_2_0) (not (currstate s_4_2)) (not (currstate s_0_0)) (not (currstate s_1_0)) (not (currstate s_3_1)) )
:effect (and (not (currstate s_2_0)) (currstate s_2_1)  (increase (total-cost) )

(:action add-drivingtest-ct6
:precondition (and (currstate s_3_1) (not (currstate s_4_2)) (not (currstate s_0_0)) (not (currstate s_1_0)) (not (currstate s_2_0)) )
:effect (and (not (currstate s_3_1)) (currstate s_3_0)  (increase (total-cost) )

(:action add-drivingtest-ct7
:precondition (and (currstate s_4_2) (currstate s_0_0) (not (currstate s_1_0)) (not (currstate s_2_0)) (not (currstate s_3_1)) )
:effect (and (not (currstate s_4_2)) (currstate s_4_0) (not (currstate s_0_0)) (currstate s_0_1)  (increase (total-cost) )

(:action add-drivingtest-ct8
:precondition (and (currstate s_4_2) (currstate s_1_0) (not (currstate s_0_0)) (not (currstate s_2_0)) (not (currstate s_3_1)) )
:effect (and (not (currstate s_4_2)) (currstate s_4_0) (not (currstate s_1_0)) (currstate s_1_1)  (increase (total-cost) )

(:action add-drivingtest-ct9
:precondition (and (currstate s_4_2) (currstate s_2_0) (not (currstate s_0_0)) (not (currstate s_1_0)) (not (currstate s_3_1)) )
:effect (and (not (currstate s_4_2)) (currstate s_4_0) (not (currstate s_2_0)) (currstate s_2_1)  (increase (total-cost) )

(:action add-drivingtest-ct10
:precondition (and (currstate s_4_2) (currstate s_3_1) (not (currstate s_0_0)) (not (currstate s_1_0)) (not (currstate s_2_0)) )
:effect (and (not (currstate s_4_2)) (currstate s_4_0) (not (currstate s_3_1)) (currstate s_3_0)  (increase (total-cost) )

(:action add-drivingtest-ct11
:precondition (and (currstate s_0_0) (currstate s_1_0) (not (currstate s_4_2)) (not (currstate s_2_0)) (not (currstate s_3_1)) )
:effect (and (not (currstate s_0_0)) (currstate s_0_1) (not (currstate s_1_0)) (currstate s_1_1)  (increase (total-cost) )

(:action add-drivingtest-ct12
:precondition (and (currstate s_0_0) (currstate s_2_0) (not (currstate s_4_2)) (not (currstate s_1_0)) (not (currstate s_3_1)) )
:effect (and (not (currstate s_0_0)) (currstate s_0_1) (not (currstate s_2_0)) (currstate s_2_1)  (increase (total-cost) )

(:action add-drivingtest-ct13
:precondition (and (currstate s_0_0) (currstate s_3_1) (not (currstate s_4_2)) (not (currstate s_1_0)) (not (currstate s_2_0)) )
:effect (and (not (currstate s_0_0)) (currstate s_0_1) (not (currstate s_3_1)) (currstate s_3_0)  (increase (total-cost) )

(:action add-drivingtest-ct14
:precondition (and (currstate s_1_0) (currstate s_2_0) (not (currstate s_4_2)) (not (currstate s_0_0)) (not (currstate s_3_1)) )
:effect (and (not (currstate s_1_0)) (currstate s_1_1) (not (currstate s_2_0)) (currstate s_2_1)  (increase (total-cost) )

(:action add-drivingtest-ct15
:precondition (and (currstate s_1_0) (currstate s_3_1) (not (currstate s_4_2)) (not (currstate s_0_0)) (not (currstate s_2_0)) )
:effect (and (not (currstate s_1_0)) (currstate s_1_1) (not (currstate s_3_1)) (currstate s_3_0)  (increase (total-cost) )

(:action add-drivingtest-ct16
:precondition (and (currstate s_2_0) (currstate s_3_1) (not (currstate s_4_2)) (not (currstate s_0_0)) (not (currstate s_1_0)) )
:effect (and (not (currstate s_2_0)) (currstate s_2_1) (not (currstate s_3_1)) (currstate s_3_0)  (increase (total-cost) )

(:action add-drivingtest-ct17
:precondition (and (currstate s_4_2) (currstate s_0_0) (currstate s_1_0) (not (currstate s_2_0)) (not (currstate s_3_1)) )
:effect (and (not (currstate s_4_2)) (currstate s_4_0) (not (currstate s_0_0)) (currstate s_0_1) (not (currstate s_1_0)) (currstate s_1_1)  (increase (total-cost) )

(:action add-drivingtest-ct18
:precondition (and (currstate s_4_2) (currstate s_0_0) (currstate s_2_0) (not (currstate s_1_0)) (not (currstate s_3_1)) )
:effect (and (not (currstate s_4_2)) (currstate s_4_0) (not (currstate s_0_0)) (currstate s_0_1) (not (currstate s_2_0)) (currstate s_2_1)  (increase (total-cost) )

(:action add-drivingtest-ct19
:precondition (and (currstate s_4_2) (currstate s_0_0) (currstate s_3_1) (not (currstate s_1_0)) (not (currstate s_2_0)) )
:effect (and (not (currstate s_4_2)) (currstate s_4_0) (not (currstate s_0_0)) (currstate s_0_1) (not (currstate s_3_1)) (currstate s_3_0)  (increase (total-cost) )

(:action add-drivingtest-ct20
:precondition (and (currstate s_4_2) (currstate s_1_0) (currstate s_2_0) (not (currstate s_0_0)) (not (currstate s_3_1)) )
:effect (and (not (currstate s_4_2)) (currstate s_4_0) (not (currstate s_1_0)) (currstate s_1_1) (not (currstate s_2_0)) (currstate s_2_1)  (increase (total-cost) )

(:action add-drivingtest-ct21
:precondition (and (currstate s_4_2) (currstate s_1_0) (currstate s_3_1) (not (currstate s_0_0)) (not (currstate s_2_0)) )
:effect (and (not (currstate s_4_2)) (currstate s_4_0) (not (currstate s_1_0)) (currstate s_1_1) (not (currstate s_3_1)) (currstate s_3_0)  (increase (total-cost) )

(:action add-drivingtest-ct22
:precondition (and (currstate s_4_2) (currstate s_2_0) (currstate s_3_1) (not (currstate s_0_0)) (not (currstate s_1_0)) )
:effect (and (not (currstate s_4_2)) (currstate s_4_0) (not (currstate s_2_0)) (currstate s_2_1) (not (currstate s_3_1)) (currstate s_3_0)  (increase (total-cost) )

(:action add-drivingtest-ct23
:precondition (and (currstate s_0_0) (currstate s_1_0) (currstate s_2_0) (not (currstate s_4_2)) (not (currstate s_3_1)) )
:effect (and (not (currstate s_0_0)) (currstate s_0_1) (not (currstate s_1_0)) (currstate s_1_1) (not (currstate s_2_0)) (currstate s_2_1)  (increase (total-cost) )

(:action add-drivingtest-ct24
:precondition (and (currstate s_0_0) (currstate s_1_0) (currstate s_3_1) (not (currstate s_4_2)) (not (currstate s_2_0)) )
:effect (and (not (currstate s_0_0)) (currstate s_0_1) (not (currstate s_1_0)) (currstate s_1_1) (not (currstate s_3_1)) (currstate s_3_0)  (increase (total-cost) )

(:action add-drivingtest-ct25
:precondition (and (currstate s_0_0) (currstate s_2_0) (currstate s_3_1) (not (currstate s_4_2)) (not (currstate s_1_0)) )
:effect (and (not (currstate s_0_0)) (currstate s_0_1) (not (currstate s_2_0)) (currstate s_2_1) (not (currstate s_3_1)) (currstate s_3_0)  (increase (total-cost) )

(:action add-drivingtest-ct26
:precondition (and (currstate s_1_0) (currstate s_2_0) (currstate s_3_1) (not (currstate s_4_2)) (not (currstate s_0_0)) )
:effect (and (not (currstate s_1_0)) (currstate s_1_1) (not (currstate s_2_0)) (currstate s_2_1) (not (currstate s_3_1)) (currstate s_3_0)  (increase (total-cost) )

(:action add-drivingtest-ct27
:precondition (and (currstate s_4_2) (currstate s_0_0) (currstate s_1_0) (currstate s_2_0) (not (currstate s_3_1)) )
:effect (and (not (currstate s_4_2)) (currstate s_4_0) (not (currstate s_0_0)) (currstate s_0_1) (not (currstate s_1_0)) (currstate s_1_1) (not (currstate s_2_0)) (currstate s_2_1)  (increase (total-cost) )

(:action add-drivingtest-ct28
:precondition (and (currstate s_4_2) (currstate s_0_0) (currstate s_1_0) (currstate s_3_1) (not (currstate s_2_0)) )
:effect (and (not (currstate s_4_2)) (currstate s_4_0) (not (currstate s_0_0)) (currstate s_0_1) (not (currstate s_1_0)) (currstate s_1_1) (not (currstate s_3_1)) (currstate s_3_0)  (increase (total-cost) )

(:action add-drivingtest-ct29
:precondition (and (currstate s_4_2) (currstate s_0_0) (currstate s_2_0) (currstate s_3_1) (not (currstate s_1_0)) )
:effect (and (not (currstate s_4_2)) (currstate s_4_0) (not (currstate s_0_0)) (currstate s_0_1) (not (currstate s_2_0)) (currstate s_2_1) (not (currstate s_3_1)) (currstate s_3_0)  (increase (total-cost) )

(:action add-drivingtest-ct30
:precondition (and (currstate s_4_2) (currstate s_1_0) (currstate s_2_0) (currstate s_3_1) (not (currstate s_0_0)) )
:effect (and (not (currstate s_4_2)) (currstate s_4_0) (not (currstate s_1_0)) (currstate s_1_1) (not (currstate s_2_0)) (currstate s_2_1) (not (currstate s_3_1)) (currstate s_3_0)  (increase (total-cost) )

(:action add-drivingtest-ct31
:precondition (and (currstate s_0_0) (currstate s_1_0) (currstate s_2_0) (currstate s_3_1) (not (currstate s_4_2)) )
:effect (and (not (currstate s_0_0)) (currstate s_0_1) (not (currstate s_1_0)) (currstate s_1_1) (not (currstate s_2_0)) (currstate s_2_1) (not (currstate s_3_1)) (currstate s_3_0)  (increase (total-cost) )

(:action add-drivingtest-ct32
:precondition (and (currstate s_4_2) (currstate s_0_0) (currstate s_1_0) (currstate s_2_0) (currstate s_3_1) )
:effect (and (not (currstate s_4_2)) (currstate s_4_0) (not (currstate s_0_0)) (currstate s_0_1) (not (currstate s_1_0)) (currstate s_1_1) (not (currstate s_2_0)) (currstate s_2_1) (not (currstate s_3_1)) (currstate s_3_0)  (increase (total-cost) )

(:action add-resit-ct33
:precondition (currstate s_1_1) 
:effect (and (not (currstate s_1_1)) (currstate s_1_0)  (increase (total-cost) )

(:action sync-driving_test_complete-t0t1
:precondition (currstate t0) 
:effect (and (not (currstate t0)) (currstate t1)))

(:action del-driving_test_complete-t0-t1
:precondition (currstate t0)
:effect (and (not (currstate t0)) (currstate t1)  (increase (total-cost) 1))
)

(:action sync-driving_test_complete-t1t2
:precondition (currstate t1) 
:effect (and (not (currstate t1)) (currstate t2)))

(:action del-driving_test_complete-t1-t2
:precondition (currstate t1)
:effect (and (not (currstate t1)) (currstate t2)  (increase (total-cost) 1))
)

(:action sync-driving_test_complete-t2t3
:precondition (currstate t2) 
:effect (and (not (currstate t2)) (currstate t3)))

(:action del-driving_test_complete-t2-t3
:precondition (currstate t2)
:effect (and (not (currstate t2)) (currstate t3)  (increase (total-cost) 1))
)

(:action sync-driving_test_complete-t3t4
:precondition (currstate t3) 
:effect (and (not (currstate t3)) (currstate t4)))

(:action del-driving_test_complete-t3-t4
:precondition (currstate t3)
:effect (and (not (currstate t3)) (currstate t4)  (increase (total-cost) 1))
)

(:action sync-driving_test_complete-t4t5
:precondition (currstate t4) 
:effect (and (not (currstate t4)) (currstate t5)))

(:action del-driving_test_complete-t4-t5
:precondition (currstate t4)
:effect (and (not (currstate t4)) (currstate t5)  (increase (total-cost) 1))
)

(:action goto-abstract_states-cs0
:precondition (and (currstate t5) (currstate s_0_0) (currstate s_1_0) (currstate s_2_0) (currstate s_3_0) (currstate s_4_0) )
:effect (and )
)

)